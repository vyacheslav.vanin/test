#pragma once
#include <memory>
#include <vvv3d/core/hal.hpp>

#include <QApplication>
#include "qtopenglwindow.h"

class HalLayer //: public vvv3d::HAL
{
public:
    HalLayer();
    ~HalLayer();

    // HAL interface
public:
    void initContext(int argc, char **argv);
    void createWindow(int width, int height, const char *name);
    //void setResizeFunction(const std::function<void (int, int)> &resizeFunction);
    //void setDisplayFunction(const std::function<void ()> &displayFunction);
    //void setIdleFunction(const std::function<void ()> &idleFunction);
    void mainLoop();
    //void stopMainLoop();
    //void swap();
    //size_t getNumScanCodes() const;
    //bool keyDown(uint16_t scancode) const;
    //bool mouseButtonDown(uint16_t button) const;
    //int getMouseX() const;
    //int getMouseY() const;
    //int getMouseRelX() const;
    //int getMouseRelY() const;
    //bool hasText() const;
    //const std::string &getText() const;
    //const std::vector<vvv3d::InputEvent> &getEvents() const;
    //void setVSync(bool vsync);
private:
    std::unique_ptr<QApplication> app;
    QtOpenGLWindow* window = nullptr;
};
