#pragma once

#include <QObject>
#include <QOpenGLFunctions>
#include <QOpenGLPaintDevice>
#include <QWindow>

class QtOpenGLWindow : public QWindow, protected QOpenGLFunctions
{
    Q_OBJECT
public:
    explicit QtOpenGLWindow(QWindow* parent = nullptr);
    ~QtOpenGLWindow() override;

    virtual void render(QPainter* painter);
    virtual void render();

    virtual void initialize();

    void setAnimating(bool animating);

    void setResizeFunction(const std::function<void(int x, int y)>& function);
    void setDisplayFunction(const std::function<void()>& function);
    void setIdleFunction(const std::function<void()>& function);
    void setVSync(bool enable);
public slots:
    void renderLater();
    void renderNow();

protected:
    bool event(QEvent* event) override;

    void exposeEvent(QExposeEvent* event) override;
    void resizeEvent(QResizeEvent* event) override;

private:
    bool m_animating;

    QOpenGLContext* m_context;
    QOpenGLPaintDevice* m_device;

    std::function<void(int x, int y)> resize_function;
    std::function<void()> display_function;
    std::function<void()> idle_function;
};
