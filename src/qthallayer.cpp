#include "qthallayer.hpp"
#include <QApplication>

HalLayer::HalLayer()
{

}

HalLayer::~HalLayer()
{
    delete window;
}


void HalLayer::initContext(int argc, char **argv)
{
    app = std::make_unique<QApplication>(argc, argv);
}

void HalLayer::createWindow(int width, int height, const char *name)
{
    window = new QtOpenGLWindow();
    window->resize(width, height);
    window->setTitle(name);
    window->show();
}

//void HalLayer::setResizeFunction(const std::function<void (int, int)> &resizeFunction)
//{
//    window->setResizeFunction(resizeFunction);
//}
//
//void HalLayer::setDisplayFunction(const std::function<void ()> &displayFunction)
//{
//    window->setDisplayFunction(displayFunction);
//}
//
//void HalLayer::setIdleFunction(const std::function<void ()> &idleFunction)
//{
//    window->setIdleFunction(idleFunction);
//}
//
void HalLayer::mainLoop()
{
    app->exec();
}
//
//void HalLayer::stopMainLoop()
//{
//    //app->exit();
//}
//
//void HalLayer::swap()
//{
//    // unused
//}
//
//size_t HalLayer::getNumScanCodes() const
//{
//    return 0;
//}
//
//bool HalLayer::keyDown(uint16_t scancode) const
//{
//    return false;
//}
//
//bool HalLayer::mouseButtonDown(uint16_t button) const
//{
//    return false;
//}
//
//int HalLayer::getMouseX() const
//{
//    return 0;
//}
//
//int HalLayer::getMouseY() const
//{
//    return 0;
//}
//
//int HalLayer::getMouseRelX() const
//{
//    return 0;
//}
//
//int HalLayer::getMouseRelY() const
//{
//    return 0;
//}
//
//bool HalLayer::hasText() const
//{
//    return 0;
//}
//
//const std::string& HalLayer::getText() const
//{
//    static const std::string empty;
//    return empty;
//}
//
//const std::vector<vvv3d::InputEvent>& HalLayer::getEvents() const
//{
//    static const std::vector<vvv3d::InputEvent> empty;
//    return empty;
//}
//
//void HalLayer::setVSync(bool vsync)
//{
//    window->setVSync(vsync);
//}
//
